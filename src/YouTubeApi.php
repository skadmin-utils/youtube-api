<?php

declare(strict_types=1);

namespace SkadminUtils\YouTubeApi;

use Nette\Utils\Json;
use Nette\Utils\JsonException;

use function count;
use function file_get_contents;
use function implode;
use function intval;
use function is_array;
use function is_string;
use function sprintf;

class YouTubeApi
{
    private const TYPE_SEARCH  = 0;
    private const TYPE_VIDEO   = 1;
    private const DEFAULT_PATH = 'https://www.googleapis.com/youtube/v3/';

    private string $googleApiKey     = '';
    private string $youtubeChannelId = '';
    private string $part             = 'snippet,id';
    private string $order            = 'date';

    public function __construct(string $googleApiKey, string $youtubeChannelId)
    {
        $this->googleApiKey     = $googleApiKey;
        $this->youtubeChannelId = $youtubeChannelId;
    }

    /**
     * @param int|string[] $videoCode
     *
     * @return YouTubeVideo[]
     *
     * @throws JsonException
     */
    public function getVideos($videoCode): array
    {
        $videoCode = is_array($videoCode) ? implode(',', $videoCode) : $videoCode;
        $query     = $this->createQuery(self::TYPE_VIDEO, null, ['id' => $videoCode]);
        $videos    = $this->getVideosFromYoutube($query);

        $youtubeVideos = [];
        foreach ($videos['items'] as $video) {
            if (! isset($video['id'])) {
                continue;
            }

            $youtubeVideos[] = $this->prepareYoutubeVideo($video['id'], $video);
        }

        return $youtubeVideos;
    }

    /**
     * @param array<mixed> $params
     */
    private function createQuery(int $type, ?int $limit = null, array $params = []): string
    {
        $path = self::DEFAULT_PATH;

        switch ($type) {
            case self::TYPE_SEARCH:
                $path .= 'search?';
                break;
            case self::TYPE_VIDEO:
                $path .= 'videos?';
                break;
        }

        $path .= sprintf('key=%s&channelId=%s&part=%s&order=%s', $this->googleApiKey, $this->youtubeChannelId, $this->part, $this->order);

        if ($limit !== null) {
            $path .= sprintf('&maxResults=%s', $limit + intval($limit / 2));
        }

        foreach ($params as $key => $value) {
            $path .= sprintf('&%s=%s', $key, $value);
        }

        return $path;
    }

    /**
     * na videa podle id
     * https://www.googleapis.com/youtube/v3/videos?categoryId=dasdas&key=AIzaSyCL93hy7UsFsdme5CYl4M38WjpeFUMK_nU&channelId=UCFELFng53-Rqy5vYPzdL7LQ&part=snippet,id&id=J1joW_U-6hc,XbTKlset-zQ
     *
     * @return mixed[]
     */
    private function getVideosFromYoutube(string $query): array
    {
        $result = @file_get_contents($query);

        if (is_string($result)) {
            return Json::decode($result, Json::FORCE_ARRAY);
        }

        return [];
    }

    /**
     * @param array<mixed> $video
     */
    private function prepareYoutubeVideo(string $id, array $video): YouTubeVideo
    {
        return new YouTubeVideo($id, $video['snippet']['title'], $video['snippet']['description']);
    }

    /**
     * @return YouTubeVideo[]
     *
     * @throws JsonException
     */
    public function search(?int $limit = null): array
    {
        $query  = $this->createQuery(self::TYPE_SEARCH, $limit);
        $videos = $this->getVideosFromYoutube($query);

        $youtubeVideos = [];
        foreach ($videos['items'] as $video) {
            if (isset($video['id']['videoId'])) {
                $youtubeVideos[] = $this->prepareYoutubeVideo($video['id']['videoId'], $video);
            }

            if (count($youtubeVideos) === $limit) {
                break;
            }
        }

        return $youtubeVideos;
    }
}
